/*
 *
 *   Maximize
 *   
 *
 */

var maximizedSection=-1;

function toggleWidth(sectionIndex){
	if (maximizedSection>0) {
		minimizeSection(sectionIndex);
	}else{
		maximizeSection(sectionIndex);
	}
}

function maximizeSection(sectionIndex){

	$('#dailyhappenings-section').removeClass('col-lg-4');
	$('#awards-section').removeClass('col-lg-4');
	$('#microevents-section').removeClass('col-lg-4');

	$('#microevents-section').hide();
	$('#awards-section').hide();
	$('#dailyhappenings-section').hide();

	switch (sectionIndex){
		case 1:
			$('#dailyhappenings-section').toggleClass('col-lg-12');
			$('#dailyhappenings-section').show();
		break;

		case 2:
			$('#awards-section').toggleClass('col-lg-12');
			$('#awards-section').show();
		break;

		case 3:
			$('#microevents-section').toggleClass('col-lg-12');
			$('#microevents-section').show();
		break;
	}
	maximizedSection=sectionIndex;
}

function minimizeSection(sectionIndex) {
	
	switch (sectionIndex){
		case 1:
			$('#dailyhappenings-section').removeClass('col-lg-12');
			$('#dailyhappenings-section').show();
		break;

		case 2:
			$('#awards-section').removeClass('col-lg-12');
			$('#awards-section').show();
		break;

		case 3:
			$('#microevents-section').removeClass('col-lg-12');
			$('#microevents-section').show();
		break;
	}

	$('#dailyhappenings-section').toggleClass('col-lg-4');
	$('#awards-section').toggleClass('col-lg-4');
	$('#microevents-section').toggleClass('col-lg-4');
	$('#dailyhappenings-section').show();
	$('#awards-section').show();
	$('#microevents-section').show();
	maximizedSection=-1;
}



jQuery(document).ready(function($) {
  $(window).scroll(function() {
    var scrollPos = $(window).scrollTop(),
        navbar = $('.navbar');

    if (scrollPos > 50) {
      navbar.addClass('alt-color');
    } else {
      navbar.removeClass('alt-color');
    }
  });
});


/* scrollbar */

$(function(){

    $('#ex4').mCustomScrollbar({
        axis:"y",
        setHeight: 250,
        theme: 'dark'
    });

});

/*icheck */ 

$(document).ready(function(){
  $('input').iCheck({
    checkboxClass: 'icheckbox_polaris',
    radioClass: 'iradio_polaris',
    increaseArea: '-10%' // optional
  });
});


