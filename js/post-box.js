/**
 * Created by root on 16/10/15.
 */

$(function(){

    var $placeholder = $('.placeholder'),
        $hint = $('.hint'),
        postFormClass = '.post-form';


    $hint.on('click',function(){
        var $el = $(this);
        var $form = $el.parent('.placeholder').next(postFormClass);

        $form.toggleClass(function(){
            return $form.hasClass('hide')? 'show' : 'hide'
        });

        $el.parent('.placeholder').toggleClass('show hide');
        $form.find('textarea').focus();
    });


    $(document).on('click', function(e){
        var $target = $(e.target);

        if($('.post-area').has($target).length === 0){
            if($('.placeholder:visible').length === 0){
                $('.placeholder').removeClass('hide').addClass('show');
                $('.post-form').removeClass('show').addClass('hide');
            }
        }

    });

    $('.pick-photo').on('click', function(e){
        e.preventDefault();
        $('.profile-pic').trigger('click');
    });
});